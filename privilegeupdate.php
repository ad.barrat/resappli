<!DOCTYPE html>
<html>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <title>update</title>
        <link rel="icon" href="./images/favicon.ico">    
    </head>
    <body>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <div class="container">
        <div id="header">
            <a href="./index.php"><img src="./images/logoPrixy-removebg-preview.png" class="rounded mx-auto d-block"></a>
        </div>
        <div id="title">
                <h2>Modification des rôles</h2>
            </div>
        <div id="body">   
            <form method="POST" action="./privilege.php">
                <?php
                session_start();
                require('db.php');
                $mail=$_POST['search'];
                $query="SELECT * FROM users WHERE USEREmail = '$mail'";
                $result=mysqli_query($connexion,$query);
                $tableau=mysqli_fetch_row($result);	
                $_SESSION['mailprivi']=$mail;
                ?>

                <div class="input-group mb-5">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-default">privilège</span>
                    </div>
                    <input value="<?php echo $tableau[10]?>"class="form-control" type="number" min="1" max="3" id="nmbMembres" name="privilege" required>
                </div>
                <div class="input-group mb-5">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-default">formateur</span>
                    </div>
                    <input value="<?php echo $tableau[9]?>"class="form-control" type="number" min="0" max="1" id="nmbMembres" name="formateur" required>
                </div>
                <input class="btn btn-primary" type="submit" name="send">
            </form>
        </div>
    </div>
    </body>
</html>