<!DOCTYPE html>
<html lang="fr">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <link rel="icon" href="./images/favicon.ico">    
        <title>privilege</title>
    </head>
    <body>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <div class="container">
        <a href="./index.php"><img src="./images/logoPrixy-removebg-preview.png" class="rounded mx-auto d-block"></a>
        <div id="body">
            <form method="POST" action="./addsalle.php" >
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-default">Nom de la salle</span>
                </div>
                <input class="form-control" type="text" name="salle" required></input>

                <input class="btn btn-primary" type="submit" name="submit">
            </form>
        </div>
    </div>
    </body>
</html>