<?php
session_start();
require('db.php');

if (!isset($_SESSION['username'])) {
    $_SESSION['msg'] = "You must log in first";
    header('location: formation_page1.php');
}
elseif($_SESSION['privilege']>2){
    header('location: index.php');
}

elseif (isset($_POST['send'])) {
    $privi=$_POST['privilege'];
    $formateur=$_POST['formateur'];
    $mail=$_SESSION['mailprivi'];
    $query="UPDATE users SET PRIVILEGEId='$privi', USERFormateur='$formateur' WHERE USEREmail='$mail'";
    $run = mysqli_query($connexion,$query) or die(mysqli_error($connexion));
    if($run){
        echo "<script>window.alert('les modifications ont bien été appliqué')</script>";
    }
}
?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <link rel="icon" href="./images/favicon.ico">    
        <title>privilege</title>

        <!-- Select2 CSS --> 
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" /> 

        <!-- jQuery --> 
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> 

        <!-- Select2 JS --> 
        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

    </head>
    <body>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <div class="container">
        <a href="./index.php"><img src="./images/logoPrixy-removebg-preview.png" class="rounded mx-auto d-block"></a>
        <div id="body">
            <form method="POST" action="./privilegeupdate.php" >
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-default">Utilisateur</span>
                </div>
                <select name="search" class="form-select" id="inputGroupSelect03">
                    <?php
                    require('db.php');
                    $query="SELECT USEREmail FROM users";
                    $result=mysqli_query($connexion,$query);
                    if($result){
                        while($row=mysqli_fetch_assoc($result)){
                            $id=$row['USEREmail'];
                            echo "<option value='".$id."'>".$id."</option>";
                        }
                    }
                    ?>
                </select>
                <input class="btn btn-primary" type="submit" name="submit">
                <script>
                    $(document).ready(function(){
 
                    // Initialize select2
                    $("#inputGroupSelect03").select2();
                    });
                </script>
            </form>
        </div>
    </div>
    </body>
</html>