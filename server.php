<?php
session_start();

// initializing variables
$username = "";
$email    = "";
$errors = array(); 

// connect to the database
require('db.php');

// REGISTER USER
if (isset($_POST['reg_user'])) {
  // receive all input values from the form
  $username = mysqli_real_escape_string($connexion, $_POST['username']);
  $email = mysqli_real_escape_string($connexion, $_POST['email']);
  $password_1 = mysqli_real_escape_string($connexion, $_POST['password_1']);
  $password_2 = mysqli_real_escape_string($connexion, $_POST['password_2']);
  $adresse = mysqli_real_escape_string($connexion, $_POST['adresse']);
  $codePostal= mysqli_real_escape_string($connexion, $_POST['codePostal']);
  $ville = mysqli_real_escape_string($connexion, $_POST['ville']);
  $pays = mysqli_real_escape_string($connexion, $_POST['pays']);
  $telFix = mysqli_real_escape_string($connexion, $_POST['telFix']);
  $telPortable = mysqli_real_escape_string($connexion, $_POST['telMobile']);

  // form validation: ensure that the form is correctly filled ...
  // by adding (array_push()) corresponding error unto $errors array
  if (empty($username)) { array_push($errors, "Username is required"); }
  if (empty($email)) { array_push($errors, "Email is required"); }
  if (empty($password_1)) { array_push($errors, "Password is required"); }
  if ($password_1 != $password_2) {
	array_push($errors, "The two passwords do not match");
  }

  // first check the database to make sure 
  // a user does not already exist with the same username and/or email
  $user_check_query = "SELECT * FROM users WHERE USERRSocial='$username' OR USEREmail='$email' LIMIT 1";
  $result = mysqli_query($connexion, $user_check_query);
  $user = mysqli_fetch_assoc($result);
  
  if ($user) { // if user exists
    if ($user['USERRSocial'] === $username) {
      array_push($errors, "Username already exists");
    }

    if ($user['USEREmail'] === $email) {
      array_push($errors, "email already exists");
    }
  }

  // Finally, register user if there are no errors in the form
  if (count($errors) == 0) {
  	$password = password_hash($password_1,PASSWORD_DEFAULT);//encrypt the password before saving in the database..........................................................

  	$query = "INSERT INTO users ( USEREmail, UserPassWord, USERRSocial, USERAdresse, USERCodePostal, USERVille, USERPays, USERTelFix, USERTelMobile,USERFormateur , PRIVILEGEId) VALUES ( '$email', '$password', '$username', '$adresse', '$codePostal', '$ville', '$pays', '$telFix', '$telPortable',0, 3)";
    
    $run = mysqli_query($connexion,$query) or die(mysqli_error($connexion));
    if($run){
        echo "Form submitted successfully";
    }
    $_SESSION['privilege'] = 3;
  	$_SESSION['username'] = $username;
    $_SESSION['email'] = $email;
  	$_SESSION['success'] = "You are now logged in";
  	header('location: index.php');
  }
}

// LOGIN USER
if (isset($_POST['login_user'])) {
  $email = mysqli_real_escape_string($connexion, $_POST['email']);
  $passwordenter = mysqli_real_escape_string($connexion, $_POST['password']);
  $query="SELECT USERPassWord FROM users WHERE USEREmail='$email'";
  $results = mysqli_query($connexion, $query);
  $row = mysqli_fetch_array($results);
  $hash=$row['USERPassWord'];
  if (empty($email)) {
  	array_push($errors, "email is required");
  }
  if (empty($passwordenter)) {
  	array_push($errors, "Password is required");
  }

  if (count($errors) == 0) {
  	$query = "SELECT * FROM users WHERE USEREmail='$email'";
  	$results = mysqli_query($connexion, $query);

    if (password_verify($passwordenter, $hash)){
      if (mysqli_num_rows($results) == 1) {
        $row = mysqli_fetch_array($results);
        $_SESSION['email'] = $email;
        $_SESSION['privilege'] = intval($row['PRIVILEGEId']);
        $_SESSION['username'] = $row['USERRSocial'];
        $_SESSION['success'] = "You are now logged in";
        if ($_SESSION['privilege']<=2){
          header('Location: ./calendrier/index.php');
        }else{
          header('Location: index.php');
        }
      }
    }else {
  		array_push($errors, "Mauvais mot de passe ou email");
  	}
    
  }
}

?>
