<?php
session_start();
if (!isset($_SESSION['username'])) {
    $_SESSION['msg'] = "You must log in first";
    header('location: formation_page1.php');
}
?>
<!DOCTYPE html>
<html>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <title>Modifier</title>
        <link rel="icon" href="./images/favicon.ico">    
    </head>
    <body>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <div class="container">
        <div id="header">
            <a href="./index.php"><img src="./images/logoPrixy-removebg-preview.png" class="rounded mx-auto d-block"></a>
        </div>
        <div id="title">
                <h2>Modification</h2>
            </div>
        <div id="body">   
            <form method="POST" action="./updatedb.php"> 
                <fieldset>
                <?php
                include 'db.php';

                $id=$_GET['updateid'];
                $query="SELECT * FROM agenda WHERE RESId='$id'";
                $result=mysqli_query($connexion,$query);
                $tableau=mysqli_fetch_row($result);	
                $datedebut=explode(" ",$tableau[3]);
                $heurfin=explode(" ",$tableau[4]);
                $_SESSION['update']=$id;
                ?>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroup-sizing-default">Nom de réservation</span>
                        </div>
                        <input class="form-control" type="text" name="motif" value="<?php echo $tableau[5]; ?>" required></input>
                    </div>


                    <div class="input-group mb-3">
                        <label class="input-group-text" for="inputGroupSelect01">Type de réservation</label><br>
                        <select name="Type_reservation" class="form-select" id="inputGroupSelect01">
                            <?php
                            if ($tableau[2]=='Reunion'){
                                echo '<option value="Reunion" selected="selected">Réunion</option><option value="Formation">Formation</option>';
                            }else{
                                echo '<option value="Reunion">Réunion</option><option value="Formation" selected="selected">Formation</option>';
                            }
                            ?>

                        </select>
                    </div> 
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroup-sizing-default">Nombre de participant</span>
                        </div>
                        <input value="<?php echo $tableau[1]?>"class="form-control" type="number" min="0" max="30" id="nmbMembres" name="NBparticipant" required>
                    </div>

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroup-sizing-default">Date de reservation</span>
                        </div>
                        <input type="date" id="nmbMembres" name="date" class="form-control" value="<?php echo $datedebut[0];?>" required>
                    </div>

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroup-sizing-default">Début de la résérvation</span>
                        </div>
                        <input type="time" id="nmbMembres" name="heureDebut" class="form-control" min="08:00" max="19:00" value="<?php echo $datedebut[1];?>" required>
                    </div>

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroup-sizing-default">Fin de la résérvation</span>
                        </div>
                        <input type="time" id="nmbMembres" name="heureFin" class="form-control" value="<?php echo $heurfin[1]?>" min="09:00" max="20:00" required>
                    </div>

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroup-sizing-default">Nom du formateur</span>
                        </div>
                        <select name="formateur" class="form-select" id="inputGroupSelect01">
                        <option selected="selected"></option>
                            <?php
                            require('db.php');
                            $query="SELECT USERRSocial FROM users WHERE USERFormateur = 1";
                            $result=mysqli_query($connexion,$query);
                            if($result){
                                while($row=mysqli_fetch_assoc($result)){
                                    $Formateur=$row['USERRSocial'];
                                    echo "<option value='".$Formateur."'>".$Formateur."</option>";
                                }
                            }
                            ?>
                        </select>
                    </div>

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroup-sizing-default">Salle</span>
                        </div>
                        <select name="salle" class="form-select" id="inputGroupSelect01">
                            <?php
                            require('db.php');
                            $query="SELECT * FROM salle";
                            $result=mysqli_query($connexion,$query);
                            if($result){
                                $tabe=intval($tableau[8]);
                                while($row=mysqli_fetch_assoc($result)){
                                    $salleid=intval($row['SALLEId']);
                                    
                                    $salle=$row['SALLENom'];
                                    if ($salleid==$tabe){
                                        echo "<option value='".$salleid."' selected='selected'>".$salle."</option>";
                                    }else{
                                        echo "<option value='".$salleid."'>".$salle."</option>";
                                    }
                                    
                                }
                            }
                            ?>
                        </select>
                    </div>

                </fieldset>
                
                <input class="btn btn-primary" type="submit" name="submit">

            </form>
        </div>
    </div>
    </body>
</html>